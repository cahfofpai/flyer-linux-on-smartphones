# Flyer zu "Linux auf Smartphones"

Lizenz: [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

Links sollten nicht inline (`[Link](https://example.com)`) gesetzt, sondern extra angegeben werden (`Link (<https://example.com>)`), damit der Flyer auch ausgedruckt werden kann